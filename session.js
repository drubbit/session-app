const db = require("./models")
const uid = require('uid-safe').sync
const moment = require('moment')
const signature = require('cookie-signature')
const { CronJob } = require('cron')
const {Op} = require('sequelize')

const sessionManager = {
	async save(sessionId, data) {
		let [session] = await db.Session.findOrCreate({
			where: {sessionId},
			defaults: {sessionId}
		})
		await session.update({data: JSON.stringify(data)})
	},

	async keepAlive(sessionId){
		await db.Session.update({sessionId}, {where: {sessionId}})
	},
	
	async delete(sessionId) {
		await db.Session.destroy({where: {sessionId}})
	},

	async getData(sessionId) {
		let session = await db.Session.findOne({where: {sessionId}})
		return (session && JSON.parse(session.data)) || {}
	},
}

function createCronJob(ttl){
	new CronJob('0 0 0 * * *', () => {
		let date = moment()
		date.subtract(ttl, 'seconds')
		db.Session.destroy({
			where: {
				updatedAt: {[Op.lt]: date.format('YYYY-MM-DD HH:mm:ss')}
			}
		})
	})
}

module.exports = (opts = {}) => {
	opts = {
		cookieName: 'session-id',
		secret: 'secret',
		ttl: 3600,
		...opts,
	}

	createCronJob(opts.ttl)

	return async (req, res, next) => {
		let cookieSessionId = req.cookies[opts.cookieName]
		let sessionId = (cookieSessionId && signature.unsign(cookieSessionId, opts.secret)) || null
		
		req.session = sessionId ? (await sessionManager.getData(sessionId)) : {}
		const _initialData = JSON.stringify(req.session)

		req.saveSession = async () => {
			let isEmpty = !req.session || Object.keys(req.session).length == 0
			if(isEmpty){
				if(cookieSessionId) res.clearCookie(opts.cookieName)
				if(sessionId) await sessionManager.delete(sessionId)
				return
			}

			if (_initialData != JSON.stringify(req.session)) {
				if(!sessionId) sessionId = uid(24)
				await sessionManager.save(sessionId, req.session)
			}else{
				await sessionManager.keepAlive(sessionId)
			}

			cookieSessionId = signature.sign(sessionId, opts.secret)
			res.cookie(opts.cookieName, cookieSessionId, { maxAge: opts.ttl * 1000 })
		}

		let end = res.end
		res.end = async (...args) => {
			await req.saveSession()
			end.apply(res, args)
		}

		next()
	} 
}