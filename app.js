require('dotenv').config()
const express = require("express")
const session = require("./session")
const cookieParser = require("cookie-parser")
const app = express()

app.use(cookieParser())

app.use(session({
	cookieName: 'sid',
	secret: 'j93ngi1quj0',
	ttl: 3600 * 4,
}))

app.get('/', (req, res, next) => {
	req.session.views = (req.session.views || 0) + 1
	res.send(`Views: ${req.session.views}`)
})

app.get('/get', (req, res, next) => {
	res.send(`Views: ${req.session.views}`)
})

app.get('/delete', (req, res, next) => {
	delete req.session.views
	res.send('Session views deleted')
})

const {PORT} = process.env
app.listen(PORT, () => {
	console.log('APP listening on port ' + PORT)
})